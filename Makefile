linq:	lexer_linq.l parser_linq.y data.h data.c write.h write.c
	bison -d parser_linq.y -Wall
	flex lexer_linq.l
	cc -g -o $@ parser_linq.tab.c lex.yy.c data.c write.c -lfl

.PHONY: clean

clean:
	rm -f linq lex.yy.c parser_linq.tab.c lexer_linq.tab.h parser_linq.tab.h
