/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_PARSER_LINQ_TAB_H_INCLUDED
# define YY_YY_PARSER_LINQ_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    SELECT = 258,                  /* SELECT  */
    ADD = 259,                     /* ADD  */
    UPDATE = 260,                  /* UPDATE  */
    CREATE = 261,                  /* CREATE  */
    REMOVE = 262,                  /* REMOVE  */
    ON = 263,                      /* ON  */
    IN = 264,                      /* IN  */
    INTO = 265,                    /* INTO  */
    NEW = 266,                     /* NEW  */
    EQUALS = 267,                  /* EQUALS  */
    FROM = 268,                    /* FROM  */
    WHERE = 269,                   /* WHERE  */
    JOIN = 270,                    /* JOIN  */
    STRING = 271,                  /* STRING  */
    INTEGER = 272,                 /* INTEGER  */
    FLOAT = 273,                   /* FLOAT  */
    BOOL = 274,                    /* BOOL  */
    TYPE = 275,                    /* TYPE  */
    LB = 276,                      /* LB  */
    RB = 277,                      /* RB  */
    LCB = 278,                     /* LCB  */
    RCB = 279,                     /* RCB  */
    QUOTE = 280,                   /* QUOTE  */
    SEMICOLON = 281,               /* SEMICOLON  */
    DOT = 282,                     /* DOT  */
    COMMA = 283,                   /* COMMA  */
    OTHER = 284,                   /* OTHER  */
    EQ = 285,                      /* EQ  */
    COMPARISON = 286,              /* COMPARISON  */
    CONTAINS = 287,                /* CONTAINS  */
    OR = 288,                      /* OR  */
    AND = 289                      /* AND  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 58 "parser_linq.y"

    char str[32];
    int number;
    int boolType;
    int binOperand;
    int valType;
    int logicOp;
    double floatValue;
    Statement *statement;
    SimpleJoin *simpleJoin;
    SelectValue *selectValue;
    SelectValueList *selectValueList;
    SelectNewValue *selectNewValue;
    ColumnDefinitionList *columnDefinitionList;
    ColumnDefinition *columnDefinition;
    InsertList *insertList;
    ColumnValueList *columnValueList;
    ColumnValue* columnValue;
    Condition *condition;
    Value *value;
    ColumnInfo *columnInfo;

#line 121 "parser_linq.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_PARSER_LINQ_TAB_H_INCLUDED  */
