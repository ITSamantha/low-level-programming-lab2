#include "data.h"
#include <stdlib.h>
#include <string.h>


Statement *createEmptyStatement(char *tableName, int type) {
    Statement *statement = (Statement *)calloc(1, sizeof(Statement));
    statement->stmtType = type;
    statement->tableName = (char *)calloc(1, strlen(tableName) + 1);
    strcpy(statement->tableName, tableName);
    return statement;
}

Value *createNewIntValue(int intValue) {
    Value *value = (Value *)calloc(1, sizeof(Value));
    value->valueType = INTEGER_TYPE;
    value->intValue = intValue;
    return value;
}

Value *createNewBoolValue(int boolValue) {
    Value *value = (Value *)calloc(1, sizeof(Value));
    value->valueType = BOOLEAN_TYPE;
    value->boolValue = boolValue;
    return value;
}

Value *createNewStringValue(char *stringValue) {
    Value *value = (Value *)calloc(1, sizeof(Value));
    value->valueType = STRING_TYPE;
    value->stringValue = (char *)calloc(1, strlen(stringValue) + 1);
    strcpy(value->stringValue, stringValue);
    return value;
}

Value *createNewFloatValue(double floatValue) {
    Value *value = (Value *)calloc(1, sizeof(Value));
    value->valueType = FLOAT_TYPE;
    value->floatValue = floatValue;
    return value;
}

SimpleJoin *createSimpleJoin(char *tableRowName, char *tableName, ColumnInfo *info1, ColumnInfo *info2) {
    SimpleJoin *simpleJoin = (SimpleJoin *)calloc(1, sizeof(SimpleJoin));
    if (tableRowName != NULL) {
        simpleJoin->tableRowName = (char *)calloc(1, strlen(tableRowName) + 1);
        strcpy(simpleJoin->tableRowName, tableRowName);
    }
    if (tableName != NULL) {
        simpleJoin->tableName = (char *)calloc(1, strlen(tableName) + 1);
        strcpy(simpleJoin->tableName, tableName);
    }
    simpleJoin->info1 = info1;
    simpleJoin->info2 = info2;
    return simpleJoin;
}

ColumnDefinition *createNewColumnDefinition(char *columnName, int type) {
    ColumnDefinition *columnDefinition = (ColumnDefinition *)calloc(1, sizeof(ColumnDefinition));
    if (columnName != NULL) {
        columnDefinition->columnName = (char *)calloc(1, strlen(columnName) + 1);
        strcpy(columnDefinition->columnName, columnName);
    }
    columnDefinition->columnType = type;
    return columnDefinition;
}

ColumnValue *createColumnValue(char *columnName, Value *value) {
    ColumnValue *columnValue = (ColumnValue *)calloc(1, sizeof(ColumnValue));
    if (columnName != NULL) {
        columnValue->columnName = (char *)calloc(1, strlen(columnName) + 1);
        strcpy(columnValue->columnName, columnName);
    }
    columnValue->value = value;
    return columnValue;
}

ColumnInfo *createColumnInfo(char *tableRowName, char *columnName) {
    ColumnInfo *columnInfo = (ColumnInfo *)calloc(1, sizeof(ColumnInfo));
    if (tableRowName != NULL) {
        columnInfo->tableRowName = (char *)calloc(1, strlen(tableRowName) + 1);
        strcpy(columnInfo->tableRowName, tableRowName);
    }
    if (columnName != NULL) {
        columnInfo->columnName = (char *)calloc(1, strlen(columnName) + 1);
        strcpy(columnInfo->columnName, columnName);
    }
    return columnInfo;
}

SelectValue *createSelectValueWithTableRowName(char *tableRowName) {
    SelectValue *selectValue = (SelectValue *)calloc(1, sizeof(SelectValue));
    selectValue->type = TABLE_ROW_NAME;
    selectValue->data.tableRowName = (char *)calloc(1, strlen(tableRowName) + 1);
    strcpy(selectValue->data.tableRowName, tableRowName);
    return selectValue;
}

SelectValue *createSelectValueWithList(SelectValueList *list) {
    SelectValue *selectValue = (SelectValue *)calloc(1, sizeof(SelectValue));
    selectValue->type = OTHER_TYPE;
    selectValue->data.list = list;
    return selectValue;
}

SelectNewValue *createSelectNewValueWithTableRowName(char *tableRowName) {
    SelectNewValue *selectNewValue = (SelectNewValue *)calloc(1, sizeof(SelectNewValue));
    selectNewValue->type = TABLE_ROW_NAME;
    selectNewValue->columnInfo = NULL;
    selectNewValue->name.tableRowName = (char *)calloc(1, strlen(tableRowName) + 1);
    strcpy(selectNewValue->name.tableRowName, tableRowName);
    return selectNewValue;
}

SelectNewValue *createSelectNewValueWithNewDef(char *newColumnName, ColumnInfo *info) {
    SelectNewValue *selectNewValue = (SelectNewValue *)calloc(1, sizeof(SelectNewValue));
    selectNewValue->type = OTHER_TYPE;
    selectNewValue->name.newColumnName = (char *)calloc(1, strlen(newColumnName) + 1);
    strcpy(selectNewValue->name.newColumnName, newColumnName);
    selectNewValue->columnInfo = info;
    return selectNewValue;
}

SelectValueList *createSelectValueList(SelectNewValue *value, SelectValueList *prev) {
    SelectValueList *selectValueList = (SelectValueList *)calloc(1, sizeof(SelectValueList));
    selectValueList->value = value;
    selectValueList->next = prev;
    return selectValueList;
}

ColumnDefinitionList *createColumnDefinitionList(ColumnDefinition *value, ColumnDefinitionList *prev) {
    ColumnDefinitionList *columnDefinitionList = (ColumnDefinitionList *)calloc(1, sizeof(ColumnDefinitionList));
    columnDefinitionList->columnDefinition = value;
    columnDefinitionList->next = prev;
    return columnDefinitionList;
}

InsertList *createInsertList(ColumnValueList *columnValueList, InsertList *prev) {
    InsertList *insertList = (InsertList *)calloc(1, sizeof(InsertList));
    insertList->list = columnValueList;
    insertList->next = prev;
    return insertList;
}

ColumnValueList *createColumnValueList(ColumnValue *value, ColumnValueList *prev) {
    ColumnValueList *columnValueList = (ColumnValueList *)calloc(1, sizeof(ColumnValueList));
    columnValueList->columnValue = value;
    columnValueList->next = prev;
    return columnValueList;
}

Condition *createNewBinaryCondition(ColumnInfo *columnInfo, int type, Value *value) {
    Condition *condition = (Condition *)calloc(1, sizeof(Condition));
    condition->conditionType = BINARY_TYPE;
    condition->right = NULL;
    condition->left = NULL;
    condition->operandType.binaryOperand = type;
    condition->value = value;
    condition->column = columnInfo;
    return condition;
}

Condition *createNewLogicCondition(int type, Condition *left, Condition *right) {
    Condition *condition = (Condition *)calloc(1, sizeof(Condition));
    condition->conditionType = LOGIC_TYPE;
    condition->right = left;
    condition->left = right;
    condition->operandType.logicOperand = type;
    condition->value = NULL;
    condition->column = NULL;
    return condition;
}

Statement *createSelectStatement(char *tableName, char *tableRowName, SimpleJoin *simpleJoin, Condition *condition, SelectValue *value) {
    Statement *statement = createEmptyStatement(tableName, STATEMENT_SELECT);
    statement->statement.selectStatement = (SelectStatement *)calloc(1, sizeof(SelectStatement));
    statement->statement.selectStatement->tableRowName = (char *)calloc(1, strlen(tableRowName) + 1);
    strcpy(statement->statement.selectStatement->tableRowName, tableRowName);
    statement->statement.selectStatement->simpleJoin = simpleJoin;
    statement->statement.selectStatement->selectValue = value;
    statement->statement.selectStatement->condition = condition;
    return statement;
}

Statement *createInsertStatement(char *tableName, InsertList *list) {
    Statement *statement = createEmptyStatement(tableName, STATEMENT_INSERT);
    statement->statement.insertStatement = (InsertStatement *)calloc(1, sizeof(InsertStatement));
    statement->statement.insertStatement->insertList = list;
    return statement;
}

Statement *createUpdateStatement(char *tableName, ColumnValueList *list, Condition *condition) {
    Statement *statement = createEmptyStatement(tableName, STATEMENT_UPDATE);
    statement->statement.updateStatement = (UpdateStatement *)calloc(1, sizeof(UpdateStatement));
    statement->statement.updateStatement->condition = condition;
    statement->statement.updateStatement->columnValueList = list;
    return statement;
}

Statement *createCreateStatement(char *tableName, ColumnDefinitionList *list) {
    Statement *statement = createEmptyStatement(tableName, STATEMENT_CREATE);
    statement->statement.createStatement = (CreateStatement *)calloc(1, sizeof(CreateStatement));
    statement->statement.createStatement->columnDefinitionList = list;
    return statement;
}

Statement *createDeleteStatement(char *tableName, Condition *condition) {
    Statement *statement = createEmptyStatement(tableName, STATEMENT_DELETE);
    statement->statement.deleteStatement = (DeleteStatement *)calloc(1, sizeof(DeleteStatement));
    statement->statement.deleteStatement->condition = condition;
    return statement;
}

void freeValue(Value *value) {
    if (value->valueType == STRING_TYPE)
        if (value->stringValue != NULL)
            free(value->stringValue);
    free(value);
}

void freeColumnInfo(ColumnInfo *columnInfo) {
    if (columnInfo->tableRowName != NULL) {
        free(columnInfo->tableRowName);
    }
    if (columnInfo->columnName != NULL) {
        free(columnInfo->columnName);
    }
    free(columnInfo);
}

void freeColumnDefinition(ColumnDefinition *columnDefinition) {
    if (columnDefinition->columnName != NULL) {
        free(columnDefinition->columnName);
    }
    free(columnDefinition);
}

void freeColumnDefinitionList(ColumnDefinitionList *columnDefinitionList) {
    ColumnDefinitionList *tmp;
    while (columnDefinitionList != NULL) {
        tmp = columnDefinitionList;
        columnDefinitionList = columnDefinitionList->next;
        if (tmp->columnDefinition != NULL) {
            freeColumnDefinition(tmp->columnDefinition);
        }
        free(tmp);
    }
}

void freeColumnValue(ColumnValue *columnValue) {
    if (columnValue->columnName != NULL) {
        free(columnValue->columnName);
    }
    if (columnValue->value != NULL) {
        freeValue(columnValue->value);
    }
    free(columnValue);
}

void freeColumnValueList(ColumnValueList *columnValueList) {
    ColumnValueList *tmp;
    while (columnValueList != NULL) {
        tmp = columnValueList;
        columnValueList = columnValueList->next;
        if (tmp->columnValue != NULL) {
            freeColumnValue(tmp->columnValue);
        }
        free(tmp);
    }
}

void freeInsertList(InsertList *insertList) {
    InsertList *tmp;
    while (insertList != NULL) {
        tmp = insertList;
        insertList = insertList->next;
        if (tmp->list != NULL) {
            freeColumnValueList(tmp->list);
        }
        free(tmp);
    }
}

void freeCondition(Condition *condition) {
    if (condition->value != NULL) {
        freeValue(condition->value);
    }
    if (condition->column != NULL) {
        freeColumnInfo(condition->column);
    }
    if (condition->left != NULL) {
        freeCondition(condition->left);
    }
    if (condition->right != NULL) {
        freeCondition(condition->right);
    }
    free(condition);
}

void freeSelectNewValue(SelectNewValue *selectNewValue) {
    if (selectNewValue->type == TABLE_ROW_NAME) {
        if (selectNewValue->name.tableRowName != NULL) {
            free(selectNewValue->name.tableRowName);
        }
    } else if (selectNewValue->type == OTHER_TYPE) {
        if (selectNewValue->name.newColumnName != NULL) {
            free(selectNewValue->name.newColumnName);
        }
        if (selectNewValue->columnInfo != NULL) {
            freeColumnInfo(selectNewValue->columnInfo);
        }
    }
    free(selectNewValue);
}

void freeSelectValueList(SelectValueList *selectValueList) {
    SelectValueList *tmp;
    while (selectValueList != NULL) {
        tmp = selectValueList;
        selectValueList = selectValueList->next;
        if (tmp->value != NULL) {
            freeSelectNewValue(tmp->value);
        }
        free(tmp);
    }
}

void freeSelectValue(SelectValue *selectValue) {
    if (selectValue->type == TABLE_ROW_NAME) {
        if (selectValue->data.tableRowName != NULL) {
            free(selectValue->data.tableRowName);
        }
    } else if (selectValue->type == OTHER_TYPE) {
        if (selectValue->data.list != NULL) {
            freeSelectValueList(selectValue->data.list);
        }
    }
    free(selectValue);
}

void freeSimpleJoin(SimpleJoin *simpleJoin) {
    if (simpleJoin->tableRowName != NULL) {
        free(simpleJoin->tableRowName);
    }
    if (simpleJoin->tableName != NULL) {
        free(simpleJoin->tableName);
    }
    if (simpleJoin->info1 != NULL) {
        freeColumnInfo(simpleJoin->info1);
    }
    if (simpleJoin->info2 != NULL) {
        freeColumnInfo(simpleJoin->info2);
    }
    free(simpleJoin);
}

void freeSelectStatement(SelectStatement *selectStatement) {
    if (selectStatement->tableRowName != NULL) {
        free(selectStatement->tableRowName);
    }
    if (selectStatement->simpleJoin != NULL) {
        freeSimpleJoin(selectStatement->simpleJoin);
    }
    if (selectStatement->condition != NULL) {
        freeCondition(selectStatement->condition);
    }
    if (selectStatement->selectValue != NULL) {
        freeSelectValue(selectStatement->selectValue);
    }
    free(selectStatement);
}

void freeInsertStatement(InsertStatement *insertStatement) {
    if (insertStatement->insertList != NULL) {
        freeInsertList(insertStatement->insertList);
    }
    free(insertStatement);
}

void freeUpdateStatement(UpdateStatement *updateStatement) {
    if (updateStatement->condition != NULL) {
        freeCondition(updateStatement->condition);
    }
    if (updateStatement->columnValueList != NULL) {
        freeColumnValueList(updateStatement->columnValueList);
    }
    free(updateStatement);
}

void freeCreateStatement(CreateStatement *createStatement) {
    if (createStatement->columnDefinitionList != NULL) {
        freeColumnDefinitionList(createStatement->columnDefinitionList);
    }
    free(createStatement);
}

void freeDeleteStatement(DeleteStatement *deleteStatement) {
    if (deleteStatement->condition != NULL) {
        freeCondition(deleteStatement->condition);
    }
    free(deleteStatement);
}

void freeStatement(Statement *statement) {
    switch (statement->stmtType) {
        case STATEMENT_SELECT:
            freeSelectStatement(statement->statement.selectStatement);
            break;
        case STATEMENT_CREATE:
            freeCreateStatement(statement->statement.createStatement);
            break;
        case STATEMENT_UPDATE:
            freeUpdateStatement(statement->statement.updateStatement);
            break;
        case STATEMENT_DELETE:
            freeDeleteStatement(statement->statement.deleteStatement);
            break;
        case STATEMENT_INSERT:
            freeInsertStatement(statement->statement.insertStatement);
            break;
    }
    free(statement->tableName);
    free(statement);
}
