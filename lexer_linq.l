%option caseless
%option noyywrap
%option yylineno

%{
#include <stdio.h>
#include <string.h>
#include "data.h"
#include "parser_linq.tab.h"
void showError(char *token);
#define YYERROR_VERBOSE 1
%}

INTEGERS ([-+]?[0-9])+
FLOATS [+-]?([0-9]*[.])?[0-9]+
WORDS ([a-zA-Z_][a-zA-Z0-9_]*)

%%

"SELECT"    {return SELECT;}
"ADD"       {return ADD;}
"UPDATE"    {return UPDATE;}
"CREATE"    {return CREATE;}
"REMOVE"    {return REMOVE;}
"ON"        {return ON;}
"IN"        {return IN;}
"INTO"      {return INTO;}
"NEW"       {return NEW;}
"EQUALS"    {return EQUALS;}
"FROM"      {return FROM;}
"WHERE"     {return WHERE;}
"JOIN"      {return JOIN;}
"&&"        {yylval.logicOp = 0; return AND;}
"||"        {yylval.logicOp = 1; return OR;}
"="         {return EQ;}
"=="        {yylval.binOperand = 0; return COMPARISON;}
"!="        {yylval.binOperand = 1; return COMPARISON;}
">"         {yylval.binOperand = 2; return COMPARISON;}
"<"         {yylval.binOperand = 3; return COMPARISON;}
">="        {yylval.binOperand = 4; return COMPARISON;}
"<="        {yylval.binOperand = 5; return COMPARISON;}
"LIKE"      {yylval.binOperand = 6; return CONTAINS;}
"INT"       {yylval.valType = 0; return TYPE;}
"STRING"    {yylval.valType = 1; return TYPE;}
"BOOL"      {yylval.valType = 2; return TYPE;}
"DOUBLE"    {yylval.valType = 3; return TYPE;}
"FALSE"     {yylval.boolType = 0; return BOOL;}
"TRUE"      {yylval.boolType = 1; return BOOL;}

"("         {return LB;}
")"         {return RB;}
"{"         {return LCB;}
"}"         {return RCB;}
"'"         {return QUOTE;}
"\""        {return QUOTE;}
";"         {return SEMICOLON;}
"."         {return DOT;}
","         {return COMMA;}

{WORDS}     {sscanf(yytext, "%s", yylval.str); return (STRING); }
{INTEGERS}  {yylval.number = atoi(yytext); return (INTEGER); }
{FLOATS}    {yylval.floatValue = atof(yytext); return (FLOAT); }
[ \t]       { /* ignore white space */ }
[\n]        {}
.           {showError(yytext); return (OTHER);}

%%

void showError(char* token) {
    printf("Unknown token: %s\n", token);
}
