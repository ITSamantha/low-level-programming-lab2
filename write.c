#include "write.h"

#define PRINT_TABS() for (int i = 0; i < tabs; i++){ printf("   "); }

int tabs = 0;


void printValue(Value* value) {
    tabs++;
    PRINT_TABS();
    printf("\"valueType\": %d,\n", value->valueType);
    PRINT_TABS();
    switch (value->valueType) {
        case INTEGER_TYPE:
            printf("\"value\": %d\n", value->intValue);
            break;
        case BOOLEAN_TYPE:
            printf("\"value\": %d\n", value->boolValue);
            break;
        case STRING_TYPE:
            printf("\"value\": \"%s\"\n", value->stringValue);
            break;
        case FLOAT_TYPE:
            printf("\"value\": %f\n", value->floatValue);
            break;
    }
    tabs--;
}

void printColumnInfo(ColumnInfo* columnInfo) {
    tabs++;
    if (columnInfo->tableRowName != NULL) {
        PRINT_TABS();
        if (columnInfo->columnName != NULL) {
            printf("\"tableRowName\": \"%s\",\n", columnInfo->tableRowName);
        } else {
            printf("\"tableRowName\": \"%s\"\n", columnInfo->tableRowName);
        }
    }
    if (columnInfo->columnName != NULL) {
        PRINT_TABS();
        printf("\"columnName\": \"%s\"\n", columnInfo->columnName);
    }
    tabs--;
}

void printColumnDefinition(ColumnDefinition* columnDefinition) {
    tabs++;
    if (columnDefinition->columnName != NULL) {
        PRINT_TABS();
        printf("\"columnName\": \"%s\",\n", columnDefinition->columnName);
    }
    PRINT_TABS();
    printf("\"columnType\": %d\n", columnDefinition->columnType);
    tabs--;
}

void printColumnDefinitionList(ColumnDefinitionList* columnDefinitionList) {
    PRINT_TABS();
    printf("\"columnDefinitionList\": [\n");
    tabs++;
    ColumnDefinitionList* tmp = columnDefinitionList;
    while (tmp != NULL) {
        PRINT_TABS();
        printf("{\n");
        printColumnDefinition(tmp->columnDefinition);
        tmp = tmp->next;
        PRINT_TABS();
        if (tmp != NULL)
            printf("},\n");
        else
            printf("}\n");
    }
    tabs--;
    PRINT_TABS();
    printf("]\n");
}

void printColumnValue(ColumnValue* columnValue) {
    tabs++;
    if (columnValue->columnName != NULL) {
        PRINT_TABS();
        printf("\"columnName\": \"%s\",\n", columnValue->columnName);
    }
    PRINT_TABS();
    printf("\"value\": {\n");
    printValue(columnValue->value);
    PRINT_TABS();
    printf("}\n");
    tabs--;
}

void printColumnValueList(ColumnValueList* columnValueList) {
    PRINT_TABS();
    printf("\"columnValueList\": [\n");
    tabs++;
    ColumnValueList* tmp = columnValueList;
    while (tmp != NULL) {
        PRINT_TABS();
        printf("{\n");
        printColumnValue(tmp->columnValue);
        tmp = tmp->next;
        PRINT_TABS();
        if (tmp != NULL)
            printf("},\n");
        else
            printf("}\n");
    }
    tabs--;
    PRINT_TABS();
    printf("]\n");
}

void printInsertList(InsertList* insertList) {
    PRINT_TABS();
    printf("\"insertList\": [\n");
    tabs++;
    InsertList* tmp = insertList;
    while (tmp != NULL) {
        PRINT_TABS();
        printf("{\n");
        tabs++;
        printColumnValueList(tmp->list);
        tmp = tmp->next;
        tabs--;
        PRINT_TABS();
        if (tmp != NULL)
            printf("},\n");
        else
            printf("}\n");
    }
    tabs--;
    PRINT_TABS();
    printf("]\n");
}

void printCondition(Condition* condition) {
    tabs++;
    PRINT_TABS();
    printf("\"conditionType\": %d,\n", condition->conditionType);
    if (condition->conditionType == LOGIC_TYPE) {
        PRINT_TABS();
        printf("\"operand\": %d,\n", condition->operandType.logicOperand);
        PRINT_TABS();
        printf("\"leftCondition\": {\n");
        printCondition(condition->left);
        PRINT_TABS();
        printf("},\n");
        PRINT_TABS();
        printf("\"rightCondition\": {\n");
        printCondition(condition->right);
        PRINT_TABS();
        printf("}\n");
    } else if (condition->conditionType == BINARY_TYPE) {
        PRINT_TABS();
        printf("\"operand\": %d,\n", condition->operandType.binaryOperand);
        PRINT_TABS();
        printf("\"value\": {\n");
        printValue(condition->value);
        PRINT_TABS();
        printf("},\n");
        if (condition->column != NULL) {
            PRINT_TABS();
            printf("\"column\": {\n");
            printColumnInfo(condition->column);
            PRINT_TABS();
            printf("}\n");
        }
    }
    tabs--;
}

void printSelectNewValue(SelectNewValue* selectNewValue) {
    tabs++;
    PRINT_TABS();
    printf("\"selectType\": \"%s\",\n", selectNewValue->type);
    if (selectNewValue->type == TABLE_ROW_NAME) {
        PRINT_TABS();
        printf("\"name\": \"%s\"\n", selectNewValue->name.tableRowName);
    } else if (selectNewValue->type == OTHER_TYPE) {
        PRINT_TABS();
        printf("\"name\": \"%s\",\n", selectNewValue->name.newColumnName);
        PRINT_TABS();
        printf("\"columnInfo\": {\n");
        printColumnInfo(selectNewValue->columnInfo);
        PRINT_TABS();
        printf("}\n");
    }
    tabs--;
}

void printSelectValueList(SelectValueList* selectValueList) {
    PRINT_TABS();
    printf("\"selectValueList\": [\n");
    tabs++;
    SelectValueList* tmp = selectValueList;
    while (tmp != NULL) {
        PRINT_TABS();
        printf("{\n");
        printSelectNewValue(tmp->value);
        tmp = tmp->next;
        PRINT_TABS();
        if (tmp != NULL)
            printf("},\n");
        else
            printf("}\n");
    }
    tabs--;
    PRINT_TABS();
    printf("]\n");
}

void printSelectValue(SelectValue* selectValue) {
    tabs++;
    PRINT_TABS();
    printf("\"selectType\": \"%s\",\n", selectValue->type);
    if (selectValue->type == TABLE_ROW_NAME) {
        PRINT_TABS();
        printf("\"name\": \"%s\"\n", selectValue->data.tableRowName);
    } else if (selectValue->type == OTHER_TYPE) {
        printSelectValueList(selectValue->data.list);
    }
    tabs--;
}

void printSimpleJoin(SimpleJoin* simpleJoin) {
    tabs++;
    PRINT_TABS();
    printf("\"tableRowName\": \"%s\",\n", simpleJoin->tableRowName);
    PRINT_TABS();
    printf("\"tableName\": \"%s\",\n", simpleJoin->tableName);
    PRINT_TABS();
    printf("\"column1\": {\n");
    printColumnInfo(simpleJoin->info1);
    PRINT_TABS();
    printf("},\n");
    PRINT_TABS();
    printf("\"column2\": {\n");
    printColumnInfo(simpleJoin->info2);
    PRINT_TABS();
    printf("}\n");
    tabs--;
}

void printSelectStatement(SelectStatement* selectStatement) {
    tabs++;
    PRINT_TABS();
    printf("\"tableRowName\": \"%s\",\n", selectStatement->tableRowName);
    if (selectStatement->simpleJoin != NULL) {
        PRINT_TABS();
        printf("\"simpleJoin\": {\n");
        printSimpleJoin(selectStatement->simpleJoin);
        PRINT_TABS();
        printf("},\n");
    }
    if (selectStatement->condition != NULL) {
        PRINT_TABS();
        printf("\"condition\": {\n");
        printCondition(selectStatement->condition);
        PRINT_TABS();
        printf("},\n");
    }
    PRINT_TABS();
    printf("\"selectedValue\": {\n");
    printSelectValue(selectStatement->selectValue);
    PRINT_TABS();
    printf("}\n");
    tabs--;
}

void printInsertStatement(InsertStatement* insertStatement) {
    tabs++;
    if (insertStatement->insertList != NULL) {
        printInsertList(insertStatement->insertList);
    }
    tabs--;
}

void printUpdateStatement(UpdateStatement* updateStatement) {
    tabs++;
    if (updateStatement->condition != NULL) {
        tabs++;
        PRINT_TABS();
        printf("\"condition\": {\n");
        printCondition(updateStatement->condition);
        PRINT_TABS();
        if (updateStatement->columnValueList != NULL) {
            printf("},\n");
        } else {
            printf("}\n");
        }
        tabs--;
    }
    if (updateStatement->columnValueList != NULL) {
        printColumnValueList(updateStatement->columnValueList);
    }
    tabs--;
}

void printCreateStatement(CreateStatement* createStatement) {
    tabs++;
    if (createStatement->columnDefinitionList != NULL) {
        printColumnDefinitionList(createStatement->columnDefinitionList);
    }
    tabs--;
}

void printDeleteStatement(DeleteStatement* deleteStatement) {
    tabs++;
    if (deleteStatement->condition != NULL) {
        PRINT_TABS();
        printf("\"condition\": {\n");
        printCondition(deleteStatement->condition);
        PRINT_TABS();
        printf("}\n");
    }
    tabs--;
}

void printStatement(Statement* statement) {
    printf("{\n");
    tabs = 1;
    PRINT_TABS();
    printf("\"tableName\": \"%s\",\n", statement->tableName);
    PRINT_TABS();
    printf("\"stmtType\": %d,\n", statement->stmtType);
    PRINT_TABS();
    printf("\"statement\": {\n");
    switch (statement->stmtType) {
        case STATEMENT_SELECT:
            printSelectStatement(statement->statement.selectStatement);
            break;
        case STATEMENT_INSERT:
            printInsertStatement(statement->statement.insertStatement);
            break;
        case STATEMENT_UPDATE:
            printUpdateStatement(statement->statement.updateStatement);
            break;
        case STATEMENT_CREATE:
            printCreateStatement(statement->statement.createStatement);
            break;
        case STATEMENT_DELETE:
            printDeleteStatement(statement->statement.deleteStatement);
            break;
    }
    PRINT_TABS();
    printf("}\n");
    tabs--;
    PRINT_TABS();
    printf("}\n");
}


